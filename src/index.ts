import check from "ssl-checker";
import chalk from "chalk";

// A list of domains that need to be checked
const domains = [
    "*.ceskehory.cz",
    "*.czeskiegory.pl",
    "*.tschechische-gebirge.de",
    "*.czech-mountains.eu",
    "*.slovenske.cz",
    "*.slovenske.sk",
    "*.slowackie.pl",
    "*.slowakische.de",
    "*.slovakian-mountains.eu",
    "*.chorvatske.cz",
    "*.hrvatska.cz",
    "*.hrvatska-smjestaj.com.hr",
    "*.chorwackie.pl",
    "*.kroatische.de",
    "*.croatian-adriatic.eu",
    "*.alpske.cz",
    "*.alpske.sk",
    "*.italske.cz",
    "www.in-alpen.de",
    "www.zitnik.cz",
    "www.ceske-hory.cz",
    "www.slowakische.at",
    "www.chorvatske.sk",
    "www.kroatische.at",
    "www.croazia-adriatico.it",
    "www.chorvatsko-forum.cz",
    "www.chorvatsko-pocasi.cz",
    "klient.ceskehory.cz",
    "client.hrvatska.cz",
    "client.kroatische.de",
    "client.chorvatske.cz",
    "red062015.eprogress.cz",
    "red062014.eprogress.cz",
    "sylwester.czeskiegory.pl",
    "silvestr.ceskehory.cz",
    "new-years-eve.czech-mountains.eu",
    "silvester.tschechische-gebirge.de",
];

// Expand domain prefixes like * or www to multiple subdomain
const subdomains = domains.flatMap(domain => {
    // *.ceskehory.cz -> [subdomain.ceskehory.cz, www.ceskehory.cz, ceskehory.cz]
    if (domain.startsWith("*.")) {
        const base = domain.replace("*.", "");
        return [
            `subdomain.${base}`,
            `www.${base}`,
            base
        ];
    }

    // www.ceskehory.cz -> [www.ceskehory.cz, ceskehory.cz]
    if (domain.startsWith("www.")) {
        return [
            domain,
            domain.replace("www.", "")
        ]
    }

    // red062015.eprogress.cz -> [red062015.eprogress.cz]
    return domain;
});

const promises = subdomains.map(async (domain) => {
    return {
        domain,
        check: await check(domain)
    }
});

Promise.all(promises).then(results => {
    console.log("\n\n");
    
    const done = results
        .map(result => {
            const valid = result.check.valid;
            const renewed = valid && result.check.daysRemaining > 30;

            const color = valid ? (renewed ? chalk.green : chalk.yellow) : chalk.red;

            const symbol = valid ? (renewed ? "🟢" : "🟡") : "🔴";
            const domain = color(result.domain.padEnd(50, " "));
            const expiration = result.check.daysRemaining.toString().padStart(4, " ") + " days remaining";

            return { status: `${symbol} ${domain} ${expiration}`, renewed };
        })
        .sort((a, b) => b.status.localeCompare(a.status))
        .map(({status, renewed}, i) => {
            process.stdout.write(status);
            process.stdout.write(i % 2 === 0 ? "     " : "\n");

            return renewed;
        })
        .every(renewed => renewed)

    console.log("\n\n");
    console.log(chalk.black(
        done
            ? chalk.bgGreen("All certificates are renewed")
            : chalk.bgYellow("Some certificates still need to be renewed")
    ));
});
